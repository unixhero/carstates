<?php

	abstract class AbstractCarState implements ICarState
	{
		/**
		 * @throws IllegalStateTransitionException
		 */
		public function engineOff()
		{
			throw new IllegalStateTransitionException;
		}

		/**
		 * @throws IllegalStateTransitionException
		 */
		public function engineOn()
		{
			throw new IllegalStateTransitionException;
		}

		/**
		 * @throws IllegalStateTransitionException
		 */
		public function driving()
		{
			throw new IllegalStateTransitionException;
		}

		/**
		 * @throws IllegalStateTransitionException
		 */
		public function parking()
		{
			throw new IllegalStateTransitionException;
		}
	}