<?php

	interface ICarState
	{
		public function engineOff();
		public function engineOn();
		public function driving();
		public function parking();
	}