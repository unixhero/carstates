<?php

	class EngineOffCarState extends AbstractCarState
	{
		/**
		 * @return EngineOnCarState
		 */
		public function engineOn()
		{
			return new EngineOnCarState();
		}
	}