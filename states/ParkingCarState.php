<?php

	class ParkingCarState extends AbstractCarState
	{
		/**
		 * @return DrivingCarState
		 */
		public function driving()
		{
			return new DrivingCarState();
		}

		/**
		 * @return EngineCarState
		 */
		public function engineOff()
		{
			return new EngineCarState();
		}
	}