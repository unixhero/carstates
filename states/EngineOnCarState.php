<?php

	class EngineOnCarState extends AbstractCarState
	{
		/**
		 * @return ParkingCarState
		 */
		public function parking()
		{
			return new ParkingCarState();
		}
	}