<?php

	class DrivingCarState extends AbstractCarState
	{
		/**
		 * @return ParkingCarState
		 */
		public function parking()
		{
			return new ParkingCarState();
		}
	}