<?php

	class IllegalStateTransitionException extends Exception
	{
		private $code;
		private $message;

		/**
		 * IllegalStateTransitionException constructor.
		 */
		public function __construct()
		{
			$this->code = 5;
			$this->message = "IllegalStateTransitionException";

			parent::__construct($this->message, $this->code);
		}

		public function __toString()
		{
			return __CLASS__ . ": ".$this->code." ".$this->message;
		}
	}