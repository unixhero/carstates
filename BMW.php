<?php

	class BMW
	{
		/**
		 * @var State
		 */
		private $state;

		/**
		 * BMW constructor.
		 *
		 * @param ICarState $startState
		 */
		public function __construct(ICarState $startState)
		{
			$this->setState($startState);
		}

		/**
		 * @param ICarState $state
		 */
		private function setState(ICarState $state)
		{
			$this->state = $state;
		}

		/**
		 * @return State
		 */
		public function getState()
		{
			return $this->state;
		}

		public function engineOff()
		{
			$this->setState($this->state->engineOff());
		}

		public function engineOn()
		{
			$this->setState($this->state->engineOn());
		}

		public function driving()
		{
			$this->setState($this->state->driving());
		}

		public function parking()
		{
			$this->setState($this->state->parking());
		}
	}